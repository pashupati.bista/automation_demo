context('Request Quotation page', () => {
  it('Requeats a quotation', () => {
    cy.login()
    // set data in available fields
    cy.get('#ui-id-2').click()
    cy.get('#quotation_breakdowncover').select('European').should('have.value', '4')
    cy.get('#quotation_windscreenrepair_t').click()
    cy.get('#quotation_incidents').type('Accident')
    cy.get('#quotation_vehicle_attributes_registration').type('09')
    cy.get('#quotation_vehicle_attributes_mileage').type(2000)
    cy.get('#quotation_vehicle_attributes_value').type(100000)
    cy.get('#quotation_vehicle_attributes_parkinglocation').select('Public place').should('have.value', 'Public place')
    cy.get('#quotation_vehicle_attributes_policystart_1i').select('2016').should('have.value', '2016')
    cy.get('#quotation_vehicle_attributes_policystart_2i').select('June').should('have.value', '6')
    cy.get('#quotation_vehicle_attributes_policystart_3i').select('7').should('have.value', '7')
    cy.get('.btn-default').click()

    cy.title().should('include', 'Insurance Broker System')
  })
})