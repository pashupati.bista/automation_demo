context('Login page', () => {
    it('Logs in with valid credentials', () => {
        cy.login()
        // assert
        cy.title().should('include', 'Insurance Broker System')
    })
})