context('Logout page', () =>{
    it('Logs out', () =>{
        cy.login()
        // logout button clicked
        cy.get('.button_to > .btn').click()
        // assert
        cy.title().should('include','Insurance Broker System')
    })
})