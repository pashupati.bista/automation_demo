context('Login page', () =>{
    it('Logs in with invalid credentials', () =>{
        // login with invalid credentials
        cy.visit('https://demo.guru99.com/insurance/v1/index.php')
        cy.get('#email').type('pashupati.bista@proshore.eu')
        cy.get('#password').type('Ad23@')
        cy.get(':nth-child(3) > .btn').click()
        // assert
        cy.title().should('include','Insurance Broker System - Login')
    })
})