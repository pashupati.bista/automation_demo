context('Edit Profile', () =>{
    it('Edits profile with valid data', () =>{
        cy.login()
        // set data in available fields
        cy.get('#editprofile').click()
        cy.get('#user_title').select('Mrs').should('have.value','Mrs')
        cy.get('#user_surname').type('Bista')
        cy.get('#user_firstname').type('Pari')
        cy.get('#user_phone').type('986546546')
        cy.get('#user_dateofbirth_1i').select('1994').should('have.value','1994')
        cy.get('#user_dateofbirth_2i').select('June').should('have.value','6')
        cy.get('#user_dateofbirth_3i').select('10').should('have.value','10')
        cy.get('[for="user_licencetype_f"]').click()
        cy.get('#user_licenceperiod').select('5').should('have.value','5')
        cy.get('#user_address_attributes_street').type('Nepal')
        cy.get('#user_address_attributes_city').type('chitwan')
        cy.get('#user_address_attributes_county').type('Nepal')
        cy.get('#user_address_attributes_postcode').type('44600')
        cy.get('#edit_user_ > .actions > .btn').click()
        // cy.get('#user_title').click

        // assert
        cy.title().should('include','Insurance Broker System')        
    })
})